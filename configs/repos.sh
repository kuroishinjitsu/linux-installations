# VLC
repository ppa:videolan/stable-daily

# Java
repository ppa:webupd8team/java

# Wine
repository ppa:ubuntu-wine/ppa

# Atom
repository ppa:webupd8team/atom

# Synapse (Applauncher)
repository ppa:synapse-core/ppa

# Android Studiop
repository ppa:paolorotolo/android-studio
