# To Auto-Accept the Oracle-License-Aggrement:
echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | sudo /usr/bin/debconf-set-selections
# And to prevent Collision with openjdk
uninstall openjdk*

# For Node-Installation
curl -sL https://deb.nodesource.com/setup_4.x | sudo -E bash -

# For Play-On-Linux Installation
wget -q "http://deb.playonlinux.com/public.gpg" -O- | sudo apt-key add -
sudo wget "http://deb.playonlinux.com/playonlinux_$(lsb_release -cs).list" -O /etc/apt/sources.list.d/playonlinux.list
