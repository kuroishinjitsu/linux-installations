install() {
sudo apt-get install $1 -y --force-yes
}
uninstall() {
sudo apt-get purge $1
}
repository() {
sudo add-apt-repository $1 -y
}
getfile() {
wget $1
}
debinstall() {
yes | sudo gdebi $1
}
upgrade() {
sudo apt-get update
sudo apt-get upgrade -y
}
nanorc() {
$1 >> ~/.nanorc
}
