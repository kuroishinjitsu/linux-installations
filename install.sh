#!/bin/sh
echo Dieses Skript ermöglicht eine unbeaufsichtigte Installation einer Sammlung von mir ausgewählter Programme.
echo Bitte geben sie anfänglich ihr SUDO-Passwort ein:
sudo -k
sudo ls
clear
. ./configs/pre-install-configs.sh
. ./configs/functions.sh
. ./configs/repos.sh
upgrade
. ./install/devtools.install.sh
. ./install/communication.install.sh
. ./install/archives.install.sh
. ./install/gimp.install.sh
. ./install/codecs.install.sh
. ./install/tools.install.sh
. ./install/media.install.sh
. ./other/npm.sh
. ./other/apm.sh
. ./deb/deb.sh
. ./configs/post-install-configs.sh
upgrade
